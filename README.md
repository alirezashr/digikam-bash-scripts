This project is for organizing facetaged images in Digikam.
Using some bash script in Digikam batch queue manager and this script we can copy a oicture to multiple directories each corresponding with one of the facetags on the image.

in Digikam I am using 
``` Batch Queue Manager config
###[r]-[date:"MMMd-yyyy"], [db:TagsPathList]
```
for file renaming which is then used in the regex for finding the pictures.

also custom bash script from base tools again in Digikam batch queue manager.
```bash
cp ${INPUT} ${OUTPUT} 
# rm ${INPUT}
```

at last we should 
```bash
find ${AlbumePath} -regextype egrep -regex '.*/[[:alnum:]]{3}-[a-zA-Z]{3}[[:digit:]]{1,2}-[[:digit:]]{4}.*' -execdir bash ${PathtoScript} {} \;
```

