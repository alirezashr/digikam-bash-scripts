#!/bin/bash
#
peopleDirectory="/mnt/WD2/Nextcloud/People/"
IFS=','
read -ra file_2_array <<< "$1"
echo ${file_2_array[@]}
IFS='_'
for part in "${file_2_array[@]}"; do
	echo $part
	#omit leading spaces
	part="${part##[[:space:]]}"
	#omit .ext
	part="${part%%.*}"
	echo "$part"
	if [[ ${part} =~ ^People ]]; then
		read -ra people <<< "${part}"
		if [[ ! -d "${peopleDirectory}${people[1]}" ]]; then
			mkdir "${peopleDirectory}${people[1]}"
		fi
		cp "$1" "${peopleDirectory}${people[1]}"
	fi
done
rm "$1"
